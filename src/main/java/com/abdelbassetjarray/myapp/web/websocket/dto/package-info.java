/**
 * Data Access Objects used by WebSocket services.
 */
package com.abdelbassetjarray.myapp.web.websocket.dto;
