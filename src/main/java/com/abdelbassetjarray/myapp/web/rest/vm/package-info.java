/**
 * View Models used by Spring MVC REST controllers.
 */
package com.abdelbassetjarray.myapp.web.rest.vm;
